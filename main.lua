resolution={1280,720}
maxFrogs=20
score = 0
buttonKeyMap={'q','1','w','2','e','3','r','4'}
frogs={}
cars={}
buttons={};
tick=0
ambientSound=audio.loadSound("73019_hanstimm_frogszm.ogg")
splatSound=audio.loadSound("Spit_Splat-Mike_Koenig-1170500447.ogg")
engineSound=audio.loadSound("319038_kwahmah-02_engine-running-loop.ogg")
engineSoundPlaying=false
engineSoundChannel=-1
cars_count=0
backgroundImage=nil

local function newFrog()
	frog=display.newImage( "frog.png" )
	frog:setFillColor(math.random()*0.25,math.random()*0.3+0.2,math.random()*0.35,1)
	frog.offsetTick=math.random()*10000;
	frog.speed=math.random()*6+1
	frog.x=(0.15+math.random()*0.7)*resolution[1]
	if math.random()>0.5 then
		frog.y=resolution[2]*9.5/10
		frog.speed=-frog.speed
	else
		frog.y=resolution[2]*0.5/10
		frog.rotation=180
	end
	frog.valid=true
	frog.cycle=math.random()*100+50;
	frog.dutycycle=(math.random()*0.1+0.2)*frog.cycle	
	frog.startTick=math.floor(tick+600*math.random())
	frog.alpha=0
	return frog
end

local function newSplat(at)
	local splat = display.newImage( "splat.png" )
	splat:toBack()
	splat.x = at.x
	splat.y = at.y
	splat.rotation=math.random()*360
	splat.TTL=200+200*math.random()
	audio.play(splatSound)
	myClosure = function() return transition.fadeOut(splat,{ time=1000,onComplete=onCompleteFade}) end
	timer.performWithDelay(3000,myClosure)
end

local function newMultiplierPopup(obj)
	multText = display.newText('x'..tostring(obj.scoreMult),obj.x,obj.y, native.systemFont,48)
	multText:setFillColor(0,0,0)
	transition.fadeOut(multText,{time=500,onComplete=onCompleteFade})
end


local function onCompleteFade(obj)
	obj:removeSelf()
end

function hslToRgb(h, s, l)
   if s == 0 then return l,l,l end
   local c = (1-math.abs(2*l-1))*s
   local x = (1-math.abs(h%2-1))*c
   local m,r,g,b = (l-.5*c), 0,0,0
   if h < 1     then r,g,b = c,x,0
   elseif h < 2 then r,g,b = x,c,0
   elseif h < 3 then r,g,b = 0,c,x
   elseif h < 4 then r,g,b = 0,x,c
   elseif h < 5 then r,g,b = x,0,c
   else              r,g,b = c,0,x
   end
   return (r+m),(g+m),(b+m)
end

local function newCar(x,y,dir)
	car=display.newImage( "car.png" )
	r,g,b=hslToRgb(math.random()*6,0.7,0.4+math.random()*0.4)
	car:setFillColor(r,g,b,1)
	if dir>0 then
		car.rotation=180
	end
	car.x=x
	car.y=y+dir*resolution[2]*0.03
	car.speed=dir*6
	car.scoreMult=1
	cars_count=cars_count+1
	if engineSoundPlaying==false then
		engineSoundPlaying=true
		engineSoundChannel=audio.play(engineSound)
		audio.fade( {volume=1, channel=engineSoundChannel, time=500 } )
	end
	return car
end

local function loop(event)
	local frog_count=0
	for i,frog in pairs(frogs) do
		if frog.startTick==tick then
			frog.alpha=1
		elseif frog.startTick<tick then
			frog_count=frog_count+1
			local currentTick=(tick+frog.offsetTick)%frog.cycle
			if currentTick<frog.dutycycle then
				frog.y=frog.y+frog.speed
				if frog.y<(resolution[2]*0.02) or frog.y>(resolution[2]*0.98) then 
					frog:removeSelf()
					frogs[i]=newFrog()
				end
			end
		end
	end
	local volume=frog_count/(maxFrogs*0.8)
	audio.setVolume(volume,{channel=frogAmbientChannel})

	for i,car in pairs(cars) do
		car.x=car.x+car.speed
		if car.x<(resolution[1]*0.12) or car.x>(resolution[1]*0.88)  then 
			car:removeSelf()
			cars[i]=NULL
			cars_count=cars_count-1
			if cars_count==0 and engineSoundPlaying==true then
				engineSoundPlaying=false
				audio.fadeOut( { channel=engineSoundChannel, time=1000 } )
			end
		else
			for j,frog in pairs(frogs) do
				dx=math.abs(frog.x-car.x)
				dy=math.abs(frog.y-car.y)
				if dx<(32+16) and dy <(16+16) then
					newSplat(frog)
					frog:removeSelf()
					frogs[j]=newFrog()
					score=score+10*car.scoreMult
					scoreText.text = 'SCORE: '..tostring(score)
					if car.scoreMult>1 then
						newMultiplierPopup(car)						
					end
					car.scoreMult=car.scoreMult+1
				end
			end
		end
	end

	tick=tick+1
	scoreText:toFront()
	backgroundImage:toBack()
end


local function onCompleteLaunchTimeout(target)
	target.active=true
	target:setFillColor(1,1,1,1)
end

local function launchCarFrom( target )
	if target.active then
		target.active=false
		target.alpha=0
		target:setFillColor(0.3,0,0,1)
		transition.fadeIn(target,{ time=2000 ,
			onComplete=onCompleteLaunchTimeout})
		cars[#cars+1]=newCar(target.x+resolution[1]*target.direction*0.1,
				target.y,target.direction)
	end
end

local function onButtonPress( event )
	if event.phase=='ended' then
		launchCarFrom(event.target)
	end
end

local function onKeyEvent( event )
	if event.phase=='up' then
		index=table.indexOf(buttonKeyMap,event.keyName)
		if index then
			launchCarFrom(buttons[index])
		end
	end
end


local function setup()
	backgroundImage = display.newImage( "background.png" )
	backgroundImage:translate( resolution[1]/2, resolution[2]/2 )

	scoreText = display.newText('SCORE: '..tostring(score) ,
		 resolution[1]/2, resolution[2]/20, native.systemFont, 48)
	scoreText:setFillColor( 1,0, 0 )

	for i=1,maxFrogs do
		frogs[i]=newFrog()
	end

	precents={22.5,37.5,62.5,77.5}
	for i=1,8 do
		buttons[i]= display.newImage( "button.png" )
		buttons[i].id=i
		buttons[i].active=true
		if (i%2==0) then
			buttons[i].direction=1
			buttons[i].x=resolution[1]/20.0
			buttons[i].y=resolution[2]*precents[math.floor((i-1)/2+1)]/100.0
		else
			buttons[i].direction=-1
			buttons[i].x= resolution[1]*19/20
			buttons[i].y= resolution[2]*precents[math.floor((i-1)/2+1)]/100.0 
		end
		buttons[i].text=display.newText(string.upper(buttonKeyMap[i]),
		 buttons[i].x, buttons[i].y, native.systemFont, 32)
		buttons[i].text:setFillColor( 0,0, 0 )
	end


	for i=1,8 do
		buttons[i]:addEventListener( "touch", onButtonPress )
	end
		
	Runtime:addEventListener("enterFrame", loop)
	Runtime:addEventListener("key", onKeyEvent)

	frogAmbientChannel=audio.play(ambientSound,{ loops=-1} )
	engineAmbientChannel=audio.play(engineSound)
	audio.setVolume(0.3,{channel=engineAmbientChannel})
end

setup()
